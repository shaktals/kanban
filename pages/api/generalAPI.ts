// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import type { NextApiRequest, NextApiResponse } from 'next'
import { v4 as uuidv4 } from 'uuid'

interface Data {
  id: string
}

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse<Data>
) {
  return await new Promise(resolve => {
    // On production I'd implement a safeParse function to safeguard this
    const body = JSON.parse(req.body)

    const payload = { ...body }
    let delay = 50

    if (req.method === 'POST') {
      payload.id = uuidv4()
      delay = 1500 // Simulate longer delay to showcase loading state
    }

    setTimeout(() => {
      res.status(200).json(payload)
      resolve(0)
    }, delay)
  })
}

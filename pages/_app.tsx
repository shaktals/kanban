import type { AppProps } from 'next/app'
import { Provider } from 'react-redux'
import { PersistGate } from 'redux-persist/integration/react'

import { store, persistor } from 'store'
import { useRootListeners } from 'lib/hooks'

import 'styles/globals.css'

function MyApp({ Component, pageProps }: AppProps) {
  useRootListeners()

  return (
    <Provider store={store}>
      <PersistGate loading={null} persistor={persistor}>
        <Component {...pageProps} />
      </PersistGate>
    </Provider>
  )
}

export default MyApp

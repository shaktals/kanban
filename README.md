This is a [Next.js](https://nextjs.org/) Typescript project bootstrapped with [`create-next-app`](https://github.com/vercel/next.js/tree/canary/packages/create-next-app).

I chose to use Next.js due to it's super powers of:

- auto code splitting
- easy route prefetching
- lots of __lazy load__ features
- super fast render times
- excellent dev experience
- if needed, ease of using SSR / static pages

## Getting Started

To run the development server:

```bash
yarn dev
```

Open [http://localhost:3000](http://localhost:3000) with your browser/mobile to see the result.

#### Code style

The project uses __absolute imports__ so that imports look cleaner.
Besides __eslint__, it also uses __prettier__ to ensure code style consistency.

As for CSS, I chose SCSS modules, that prevents clashing of classes, and offers all of SASS perks.

#### Drag and drop lib choice

After a quick research, aside some small projects, I found 3 main DnD libraries:

- react-beaufitul-dnd: not being actively developed, now on maintenance mode only
- react-dnd: seems well used, good clear docs, offers hooks, but it seems like maintenance is not optimal, mostly just one dev working on it
- react-grid-layout: look very complete, good support for responsiveness, even though lots of the features aren't needed for this project, it is designed for the React class paradigm, doesn't offer hooks, but the APIs learning curve is gentler than react-dnd

None is a perfect fit, so I considered a few options that could be adopted in the future to overcome shortcomings:

- fork react-dnd and take care of updates and fixes by ourselves
- create custom hooks for react-grid-layout to abstract the UI calculations that seem to be needed to each component

As per below comments, given by this [logrocket article](https://blog.logrocket.com/drag-and-drop-react-dnd/)'s author

"__In my opinion, React-Grid-Layout provides an unattractive API that also requires you to perform a lot of calculations on your own.__"

There were also the options to use __gridstak__ or __gridster__, but __react-grid-layout__ is specifically written for React, doesn't require other libs (like jQuery) and as mentioned above supports responsive UI and breakpoints.

So, given the intial context, **react-grid-layout** seemed like the best choice.

...

However, after playing aroung with the chosen lib for a while (as per __rgl-test__ branch), trying to organize the card elements into columns started feeling like a challenge.

So I decided to give __react-dnd__ a try.

---

After successfully visualizing the chosen library drag'n'drop functionality, I started implementing the store, and couldn't help but to do it properly. However mocking API calls ended up being a massive time drag, as many more details had to be taken into consideration, besides several more type constraints to be handled. Specially the ActionCreator type, that ideally should receive more time to be perfectly typed, which it is not right now.

#### Cards order

Through quick initial thinking and iteration, I ended up having a single list of cards, and managing their positions via their indices on that single list. However, on a real API implementation, I reckon it would be better to handle indices on a by column basis.

#### Final considerations

The protype is definetely not production ready.
Some of the improvement points are:

- Better types, especially for the redux-thunk action creators
- Implementation of a nice drag item preview, which adds a smooth touch on both desktop and mobile
- Improve UI/UX of the card/column creation dialog on mobile
  a) now the keyboard goes down when user clicks 'create',
  b) also the description could be cleared if submit is successful
  c) the height/position of modal must be improved when keyboard is up
- Tests coverage could be improved, test setup has to be fixed
  (I got jest to play nice with @testing-library and Next.js on jest-setup branch, then I saw it broke Next build)

If I had to start again, I'd not implement all the API handling methods that were not asked for (although essential to a real world app). I ended up spending quite some time there, and it brought an extra level of complexity to the typing, which was not needed for this.

#### Live preview

A live preview of the app can be seen on [Netlify](https://poetic-gelato-7d7dce.netlify.app/)

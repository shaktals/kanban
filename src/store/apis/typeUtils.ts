interface StoreTypesList {
  [key: string]: string
}

export function getTypesMap(arr: string[], fullMap?: boolean) {
  const fullList: StoreTypesList = {}

  arr.forEach((type: string) => {
    fullList[type] = type
    if (fullMap) {
      fullList[`${type}_REQUEST`] = `${type}_REQUEST`
      fullList[`${type}_SUCCESS`] = `${type}_SUCCESS`
      fullList[`${type}_ERROR`] = `${type}_ERROR`
    }
  })

  return fullList
}

// Intentional declaration of new function
// to avoid the need for a potentially obscure fullMap boolean param above
export const getFullTypesMap = (arr: string[]) => getTypesMap(arr, true)

export const getDispatchTypes = (actionType: string) => ({
  success: `${actionType}_SUCCESS`,
  error: `${actionType}_ERROR`,
  req: `${actionType}_REQUEST`,
})

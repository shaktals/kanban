export const getEndpoints = () => {
  // Temporary hack for this prototype development only.
  // If we were to streamline long term development
  // a better solution for switching test/prod APIs would be needed.
  const baseUrl = window.location.href

  return {
    NEW_COLUMN: `${baseUrl}/api/generalAPI`,
    NEW_CARD: `${baseUrl}/api/generalAPI`,
    UPDATE_CARD: `${baseUrl}/api/generalAPI`,
  }
}

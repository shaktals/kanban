import { handleJsonResp } from './respHandler'
import { getDispatchTypes } from './typeUtils'
import { Action, ReqResult } from 'lib/models'

interface ReqProps {
  url: string
  options: RequestInit
  baseType: string
  message?: string
  dispatch: (arg: Action) => void
  params?: {
    [key: string]: string
  }
}

type ReqSign = (arg: ReqProps) => Promise<ReqResult>

export const basicReqWithDispatch: ReqSign = async props => {
  const { url, options, baseType, dispatch, message, params = {} } = props

  const actionTypes = getDispatchTypes(baseType)
  dispatch({ type: actionTypes.req })

  const fullUrl = new URL(url)
  for (const key in params) {
    if (params[key]) fullUrl.searchParams.append(key, params[key])
  }

  return await fetch(fullUrl.toString(), options)
    .then(handleJsonResp)
    .then(data => {
      dispatch({
        type: actionTypes.success,
        payload: data,
        message,
      })

      return { data }
    })
    .catch(error => {
      console.error(error)

      dispatch({
        type: actionTypes.error,
        message: error?.message,
      })

      return { error }
    })
}

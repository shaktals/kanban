import { ActionProp } from 'lib/models'
import { RootState } from 'store/reducer'

export const cardsSelector = (state: RootState) => {
  const searchString = state.display.search
  if (!searchString) return state.card.list

  return state.card.list.filter(card => {
    const dsc = card.description.toLocaleLowerCase()
    return dsc.includes(searchString.toLocaleLowerCase())
  })
}

export const cardByIdSelector = (state: RootState, id: ActionProp) =>
  state.card.list.find(card => card.id === id)

import types from './types'
import { ActionCreator } from 'lib/models'
import { basicReqWithDispatch, getEndpoints } from 'store/apis'
import { cardByIdSelector } from './selectors'

export const createCard: ActionCreator = card => async dispatch => {
  // Has to be called as a function to avoid SSR incompatibility
  const endpoints = getEndpoints()

  const { error, data } = await basicReqWithDispatch({
    url: endpoints.NEW_CARD,
    options: {
      method: 'POST',
      body: JSON.stringify(card),
    },
    baseType: types.CREATE_CARD,
    message: 'Success creating card!',
    dispatch,
  })

  return { error, data }
}

export const updateCard: ActionCreator =
  props => async (dispatch, getState) => {
    // Has to be called as a function to avoid SSR incompatibility
    const endpoints = getEndpoints()

    // To save card index order to server, we should also read newCardIndex here
    // But that would entail updates in several places, and as time is running short
    // so I'll just leave this TODO comment here
    const { id: cardId, description, newColIndex } = props

    if (typeof newColIndex === 'number') {
      const columns = getState().column.list

      const index = newColIndex
      const colId = columns[index].id

      dispatch({
        type: types.UPDATE_CARD_COL,
        payload: { cardId, colId },
      })
    }

    // Select updated card data, that might have been reordered
    const card = cardByIdSelector(getState(), cardId)
    const newCard = { ...card }
    if (typeof description === 'string') newCard.description = description

    const { error, data } = await basicReqWithDispatch({
      url: endpoints.UPDATE_CARD,
      options: {
        method: 'PUT',
        body: JSON.stringify(newCard),
      },
      baseType: types.UPDATE_CARD,
      dispatch,
    })

    return { error, data }
  }

export const reorderCardDisplay: ActionCreator =
  props => async (dispatch, getState) => {
    const { dragIndex, hoverIndex, dragColIndex, colIndex } = props

    let newColId
    if (dragColIndex !== colIndex && typeof colIndex === 'number') {
      const columns = getState().column.list
      newColId = columns[colIndex].id
    }

    dispatch({
      type: types.REORDER_CARDS,
      payload: { dragIndex, hoverIndex, newColId },
    })

    return {}
  }

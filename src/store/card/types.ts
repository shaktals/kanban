import { getFullTypesMap, getTypesMap } from 'store/apis'

const fullTypes = getFullTypesMap(['CREATE_CARD', 'UPDATE_CARD'])

const types = getTypesMap(['EDIT_CARD', 'REORDER_CARDS', 'UPDATE_CARD_COL'])

export const cardTypes = { ...fullTypes, ...types }

export default cardTypes

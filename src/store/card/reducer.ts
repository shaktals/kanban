import types from './types'

import { Action, CardModel } from 'lib/models'

interface CardState {
  list: CardModel[]
}

export const cardInitialState: CardState = {
  list: [],
}

export const cardReducer = (state = cardInitialState, action: Action) => {
  switch (action.type) {
    case types.CREATE_CARD_SUCCESS: {
      const list = [...state.list, action.payload]
      return { ...state, list }
    }
    case types.UPDATE_CARD_COL: {
      const { cardId, colId } = action.payload
      const list = [...state.list]

      const cardIndex = list.findIndex(card => card.id === cardId)
      if (cardIndex < 0) return state

      const card = { ...list[cardIndex] }
      card.parentId = colId
      list.splice(cardIndex, 1)
      list.push(card)

      return { ...state, list }
    }
    case types.REORDER_CARDS: {
      const { dragIndex, hoverIndex, newColId } = action.payload
      const list = [...state.list]
      const el = list[dragIndex]

      if (!el) return state

      const draggedEl = { ...el }

      if (newColId) draggedEl.parentId = newColId

      list.splice(dragIndex, 1)
      list.splice(hoverIndex, 0, draggedEl)

      return { ...state, list }
    }
    case types.UPDATE_CARD_ERROR:
      // We're optimistically assuming above ops will succeed
      // so we should create and cache rollback state for each of those
      // and upon error we'd rollback to that state and alert user
      return state
    case types.UPDATE_CARD_SUCCESS:
      // Clean up the rollback state cache for this op
      return state
    case types.EDIT_CARD: {
      const { value, cardIndex } = action.payload

      const list = [...state.list]
      const card = { ...list[cardIndex] }
      card.description = value
      list[cardIndex] = card

      return { ...state, list }
    }
    default:
      return state
  }
}

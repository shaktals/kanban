import { combineReducers, Reducer } from 'redux'
import storage from 'redux-persist/lib/storage' // defaults to localStorage for web
import {
  persistReducer,
  FLUSH,
  REHYDRATE,
  PAUSE,
  PERSIST,
  PURGE,
  REGISTER,
} from 'redux-persist'

import { Action } from 'lib/models'

import {
  displayReducer,
  displayTypes,
  displayInitialState,
} from 'store/display'
import { columnReducer, columnInitialState } from 'store/column'
import { cardReducer, cardInitialState } from 'store/card'

const appReducer = combineReducers({
  card: cardReducer,
  column: columnReducer,
  display: displayReducer,
})

type SliceFactory<T> = ReturnType<(...args: any) => T>

export interface RootState {
  card: SliceFactory<typeof cardInitialState>
  column: SliceFactory<typeof columnInitialState>
  display: SliceFactory<typeof displayInitialState>
}

const rootReducer = (state: RootState, action: Action) => {
  if (action.type === displayTypes.CLEAR_STORE) {
    storage.removeItem('persist:root')
    return appReducer(undefined, action)
  }

  return appReducer(state, action)
}

const persistConfig = {
  key: 'root',
  storage: storage,
  whitelist: ['card', 'column'],
}

const reducer = persistReducer(persistConfig, rootReducer as Reducer)
const ignoredActions = [FLUSH, REHYDRATE, PAUSE, PERSIST, PURGE, REGISTER]

export { reducer, ignoredActions }

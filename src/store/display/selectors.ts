import { RootState } from 'store/reducer'

export const selectModal = (state: RootState) => state.display.modal

export const isLoadingSelector = (state: RootState, actions: string[]) =>
  actions.some(action => state.display.loading[action])

export const hasErrorSelector = (state: RootState, actions: string[]) => {
  const errorAction = actions.find(action => state.display.errors[action])
  return errorAction && state.display.errors[errorAction]
}

const hasDisplaySuccess = (state: RootState) => (action: string) => state.display.success[action]

export const hasSuccessMessageSelector = (state: RootState, actions: string[]) => {
  const successAction = actions.find(hasDisplaySuccess(state))
  return successAction && state.display.success[successAction]
}

const hasActionsSuccess = (state: RootState, actions: string[]) => {
  const successes = actions.filter(hasDisplaySuccess(state))
  return successes.length === actions.length
}

export const getDisplayStateSelector = (action: string) => (state: RootState) => ({
  isSuccess: hasActionsSuccess(state, [action]),
  isLoading: isLoadingSelector(state, [action]),
  message: hasSuccessMessageSelector(state, [action]),
  error: hasErrorSelector(state, [action]),
})

import { getTypesMap } from 'store/apis'

export const displayTypes = getTypesMap([
  'CLEAR_ERRORS',
  'CLEAR_SUCCESS',
  'CLEAR_STORE',
  'SHOW_MODAL',
  'CLOSE_MODAL',
  'UPDATE_SEARCH_STRING',
])

export default displayTypes

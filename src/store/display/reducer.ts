import { Action, ModalType } from 'lib/models'
import types from './types'

interface StateObj {
  [key: string]: string | boolean
}

interface ModalObj {
  type?: ModalType
  parentId?: string
  parentDescription?: string
}

interface InitialState {
  errors: StateObj
  loading: StateObj
  success: StateObj
  modal: ModalObj
  search: string
}

export const displayInitialState: InitialState = {
  errors: {},
  loading: {},
  success: {},
  modal: {},
  search: '',
}

export function displayReducer(state = displayInitialState, action: Action) {
  const { type, message } = action
  const { loading, errors, success } = state

  const matches = /(.*)_(REQUEST|SUCCESS|ERROR)/.exec(type)

  switch (true) {
    case type === types.CLEAR_ERRORS:
      return { ...state, errors: {} }
    case type === types.CLEAR_SUCCESS: {
      const rootActions = action.payload
      const success = { ...state.success }

      rootActions.forEach((rtAct: string) => {
        success[rtAct] = false
      })

      return { ...state, success }
    }
    case type === types.SHOW_MODAL:
      return { ...state, modal: action.payload }
    case type === types.CLOSE_MODAL:
      return { ...state, modal: {} }
    case type === types.UPDATE_SEARCH_STRING:
      return { ...state, search: action.payload }
    case Boolean(matches): {
      const [, actionName, actionState] = matches as Array<string>

      const isRequest = actionState === 'REQUEST'
      const isSuccess = actionState === 'SUCCESS'
      const isError = actionState === 'ERROR'

      return {
        ...state,
        loading: {
          ...loading,
          [actionName]: isRequest,
        },
        errors: {
          ...errors,
          [actionName]: isError && (message || true),
        },
        success: {
          ...success,
          [actionName]: isSuccess && (message || true),
        },
      }
    }
    default:
      return state
  }
}

import { ModalPayload } from 'lib/models'
import types from './types'

export const showModal = (payload: ModalPayload) => ({
  type: types.SHOW_MODAL,
  payload,
})

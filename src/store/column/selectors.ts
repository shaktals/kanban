import { RootState } from 'store/reducer'

export const selectColumns = (state: RootState) => state.column.list

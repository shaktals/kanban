import { Action, ColumnModel } from 'lib/models'
import types from './types'

interface ColInitialState {
  list: ColumnModel[]
}

export const columnInitialState: ColInitialState = {
  list: [],
}

export const columnReducer = (state = columnInitialState, action: Action) => {
  switch (action.type) {
    case types.CREATE_COLUMN_SUCCESS: {
      const list = [...state.list, action.payload]
      return { ...state, list }
    }
    default:
      return state
  }
}

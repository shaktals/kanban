import { getFullTypesMap } from 'store/apis'

export const columnTypes = getFullTypesMap([
  'CREATE_COLUMN',
  'DELETE_COLUMN',
  'MOVE_COLUMN',
])

export default columnTypes

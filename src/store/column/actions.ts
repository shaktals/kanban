import types from './types'
import { ActionCreator } from 'lib/models'
import { basicReqWithDispatch, getEndpoints } from 'store/apis'

export const createColumn: ActionCreator = props => async dispatch => {
  // Has to be called as a function to avoid SSR incompatibility
  const endpoints = getEndpoints()
  const { title, description } = props

  const { error, data } = await basicReqWithDispatch({
    url: endpoints.NEW_COLUMN,
    options: {
      method: 'POST',
      body: JSON.stringify({ title, description }),
    },
    baseType: types.CREATE_COLUMN,
    message: 'Success creating column!',
    dispatch,
  })

  return { error, data }
}

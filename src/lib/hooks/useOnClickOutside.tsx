import { useEffect } from 'react'

interface OnClickOutsideProps {
  onClick: () => void
  ref: React.RefObject<HTMLElement>
}

type OnClickOutsideSign = (arg: OnClickOutsideProps) => void

export const useOnClickOutside: OnClickOutsideSign = ({ onClick, ref }) => {
  useEffect(() => {
    function handleClickOutside(event: Event) {
      if (ref?.current && !ref.current.contains(event.target as HTMLElement)) {
        onClick()
      }
    }
    document.addEventListener('mousedown', handleClickOutside)
    return () => {
      document.removeEventListener('mousedown', handleClickOutside)
    }
  }, [onClick, ref])
}

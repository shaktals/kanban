import { TimeoutId } from '@reduxjs/toolkit/dist/query/core/buildMiddleware/types'
import { useEffect, useCallback } from 'react'
import { useRouter } from 'next/router'
import { useDispatch, useSelector } from 'react-redux'

import { getDisplayStateSelector, displayTypes } from 'store/display'

interface UseDisplayStateProps {
  actionType: string
  delay?: number
  redirectPath?: string
}

export const useDisplayState = ({
  actionType,
  delay = 3000,
  redirectPath,
}: UseDisplayStateProps) => {
  const dispatch = useDispatch()
  const router = useRouter()

  const selector = getDisplayStateSelector(actionType)
  const { message, error, isLoading, isSuccess } = useSelector(selector)

  useEffect(() => {
    if (redirectPath) router.prefetch(redirectPath)
  }, [redirectPath, router])

  const resetSuccessStatus = useCallback(() => {
    dispatch({ type: displayTypes.CLEAR_SUCCESS, payload: [actionType] })
  }, [dispatch, actionType])

  useEffect(() => {
    let timeout: TimeoutId

    if (isSuccess) {
      timeout = setTimeout(() => {
        if (redirectPath) router.push(redirectPath)
        resetSuccessStatus()
      }, delay)
    }

    return () => clearTimeout(timeout)
  }, [isSuccess, redirectPath, router, resetSuccessStatus, delay])

  // Used to reset the status if the user navigates out before timeout
  useEffect(resetSuccessStatus, [resetSuccessStatus])

  return { message, error, isLoading, isSuccess }
}

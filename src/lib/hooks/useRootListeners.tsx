import { useEffect } from 'react'

// To prevent navigating to the previous page upon backspace press
const keyDownFn = (e: KeyboardEvent) => {
  const { nodeName, type } = e.target as HTMLInputElement

  const isInput = ['input', 'textarea'].includes(nodeName.toLowerCase())

  if (!isInput && type !== 'text' && e.which === 8) e.preventDefault()
}

export const useRootListeners = () => {
  useEffect(() => {
    window.addEventListener('keydown', keyDownFn)

    return () => window.removeEventListener('keydown', keyDownFn)
  }, [])
}

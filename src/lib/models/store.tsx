import { Dispatch } from 'redux'

import { TruthyString } from './general'
import { RootState } from 'store/reducer'

export interface Action {
  type: string
  payload?: any
  message?: string
}

export interface GeneralAction {
  [key: string]: any
}

export type ActionProp = TruthyString | number | undefined

export interface GenericEntity {
  [key: string]: ActionProp
}

export interface ReqResult {
  data?: object
  error?: Error
}

export type ActionModel = (
  arg0: Dispatch,
  arg1: () => RootState
) => Promise<ReqResult>

export type ActionCreator = (arg: GenericEntity) => ActionModel | Action

// At one point I had some vars called 'task...' and others 'card...'
// I chose 'card' because it's more generic
export interface NewCardModel {
  description: string
  parentId: string
}

export interface CardModel extends NewCardModel {
  id: string
}

export interface ColumnModel {
  id: string
  description: string
}

export interface ModalPayload {
  type: string
  parentId?: string
  parentDescription?: string
}

export type ModalType = 'column' | 'card' | undefined

export interface DragItem {
  id?: string
  type?: string
  index: number
  colIndex: number
  cardId?: string
}

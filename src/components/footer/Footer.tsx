import { NextPage } from 'next'
import Image from 'next/image'

import stl from './Footer.module.scss'

const Footer: NextPage = () => (
  <footer className={stl.footer}>
    <a
      href="https://zerohash.com"
      target="_blank"
      rel="noopener noreferrer"
      className={stl.linkElement}
    >
      Powered by{' '}
      <span className={stl.logo}>
        <Image src="/logo.jpg" alt="Vercel Logo" height={30} width={95} />
      </span>
    </a>
  </footer>
)

export default Footer

import stl from './Loading.module.scss'

interface LoadingProps {
  show: boolean
}

const LoadingIndicator = ({ show }: LoadingProps) =>
  show ? <div className={stl.typing} /> : null

export default LoadingIndicator

import { ChangeEventHandler } from 'react'
import { useDispatch, useSelector } from 'react-redux'

import CloseIcon from 'assets/close.svg'

import { RootState } from 'store/reducer'
import { displayTypes } from 'store/display'

import stl from './SearchBar.module.scss'

const SearchBar = () => {
  const dispatch = useDispatch()
  const searchString = useSelector((state: RootState) => state.display.search)

  const handleChange: ChangeEventHandler<HTMLInputElement> = e =>
    dispatch({
      type: displayTypes.UPDATE_SEARCH_STRING,
      payload: e.target.value,
    })

  const onClearPress = () =>
    dispatch({
      type: displayTypes.UPDATE_SEARCH_STRING,
      payload: '',
    })

  return (
    <div className={stl.container}>
      <input
        type="text"
        placeholder="Search cards"
        className={stl.inputClass}
        onChange={handleChange}
        value={searchString}
      />
      <button className={stl.closeButton} onClick={onClearPress}>
        <CloseIcon className={stl.closeIcon} />
      </button>
    </div>
  )
}

export default SearchBar

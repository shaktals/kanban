import { useEffect, useRef } from 'react'
import clsx from 'clsx'

import ModalPortal from './ModalPortal'

import { useOnClickOutside } from 'lib/hooks'

import stl from './Modal.module.scss'

type ModalSign = (arg: {
  isOpen: boolean
  close: () => void
  children: React.ReactNode
  customClass?: string
}) => JSX.Element | null

const Modal: ModalSign = ({ isOpen, close, children, customClass }) => {
  const contentRef = useRef<HTMLDivElement>(null)

  useOnClickOutside({
    ref: contentRef,
    onClick: close,
  })

  useEffect(() => {
    if (!isOpen) return

    contentRef.current?.focus()
  }, [isOpen])

  if (!isOpen) return null

  return (
    <ModalPortal selector="#modal">
      <div className={clsx(stl.background, customClass)}>
        <div ref={contentRef}>{children}</div>
      </div>
    </ModalPortal>
  )
}

export default Modal

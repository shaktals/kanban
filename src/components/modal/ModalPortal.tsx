import { useRef, useEffect, useState } from 'react'
import { createPortal } from 'react-dom'

interface ModalProps {
  children: React.ReactNode
  selector: string
}

type ModalSign = (arg: ModalProps) => JSX.Element | null

const ModalPortal: ModalSign = ({ children, selector }) => {
  const ref = useRef<HTMLDivElement | null>(null)
  const [mounted, setMounted] = useState(false)

  useEffect(() => {
    ref.current = document.querySelector(selector)
    setMounted(true)
  }, [selector])

  return mounted ? createPortal(children, ref.current as Element) : null
}

export default ModalPortal

import clsx from 'clsx'

import AddIcon from 'assets/add.svg'

import stl from './AddButton.module.scss'

interface AddButtonProps {
  onButtonClick: () => void
  customClass?: string
  size?: number
  name: string
}

const AddButton = ({
  onButtonClick,
  customClass,
  size = 40,
  name,
}: AddButtonProps) => (
  <button
    type="button"
    name={name}
    className={clsx(stl.addButton, customClass)}
    onClick={onButtonClick}
  >
    <AddIcon className={stl.addIcon} width={size} height={size} />
  </button>
)

export default AddButton

import { useMemo } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { DndProvider } from 'react-dnd'
import { TouchBackend } from 'react-dnd-touch-backend'
import { HTML5Backend } from 'react-dnd-html5-backend'

import Modal from 'components/modal'
import InputDialog from 'components/input-dialog'

import { createCard } from 'store/card'
import { createColumn } from 'store/column'
import { displayTypes, selectModal } from 'store/display'
import { ModalType } from 'lib/models'
import { entityTypes } from 'lib/constants'
import { isMobile } from 'lib/utils'

interface DndWrapperProps {
  children: React.ReactNode
}

const DndWrapper = ({ children }: DndWrapperProps) => {
  const dispatch = useDispatch()
  const {
    type: modalType,
    parentId,
    parentDescription,
  } = useSelector(selectModal)
  const dispatchCloseModal = () => dispatch({ type: displayTypes.CLOSE_MODAL })

  const actionCreator = useMemo(() => getActionCreator(modalType), [modalType])

  return (
    <DndProvider backend={isMobile() ? TouchBackend : HTML5Backend}>
      {children}
      <Modal isOpen={!!modalType} close={dispatchCloseModal}>
        <InputDialog
          inputType={modalType}
          action={actionCreator}
          parentId={parentId}
          parentDescription={parentDescription}
        />
      </Modal>
    </DndProvider>
  )
}

const getActionCreator = (inputType: ModalType) => {
  switch (inputType) {
    case entityTypes.COLUMN:
      return createColumn
    case entityTypes.CARD:
      return createCard
    default:
      return createCard
  }
}

export default DndWrapper

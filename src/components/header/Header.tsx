import { useState } from 'react'
import { useDispatch } from 'react-redux'

import AddButton from 'components/add-button'
import SearchBar from 'components/search-bar'

import { showModal } from 'store/display'

import stl from './Header.module.scss'

const ActionsDropdown = () => {
  const dispatch = useDispatch()

  const addColumn = () => dispatch(showModal({ type: 'column' }))

  return (
    <div className={stl.header}>
      <span className={stl.title}>Kanban</span>
      <SearchBar />
      <AddButton size={28} onButtonClick={addColumn} name="add-button" />
    </div>
  )
}

export default ActionsDropdown

import { useDrop } from 'react-dnd'

import { cardTypes } from 'lib/constants'
import { DragItem } from 'lib/models'

import stl from './Column.module.scss'

interface DCWrapProps {
  colIndex: number
  children: React.ReactNode
}

const DroppableColumnWrapper = ({ colIndex, children }: DCWrapProps) => {
  const [, dropRef] = useDrop(
    () => ({
      accept: cardTypes.TASK,
      drop: (item: DragItem) => ({ colIndex, newCardIndex: item.index }),
    }),
    [colIndex]
  )

  return (
    <div className={stl.column} ref={dropRef}>
      {children}
    </div>
  )
}

export default DroppableColumnWrapper

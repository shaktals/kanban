import { useDispatch, useSelector } from 'react-redux'

import { ColumnModel, CardModel } from 'lib/models'

import Card from 'components/card'
import AddButton from 'components/add-button'
import DroppableColumnWrapper from './DropColumn'

import { cardsSelector } from 'store/card'
import { showModal } from 'store/display'

import stl from './Column.module.scss'

interface ColumnProps extends ColumnModel {
  colIndex: number
}

const Column = ({ id, description, colIndex }: ColumnProps) => {
  const dispatch = useDispatch()

  const addCard = () => {
    const payload = {
      type: 'card',
      parentId: id,
      parentDescription: description,
    }

    dispatch(showModal(payload))
  }

  const cards = useSelector(cardsSelector)

  const renderCards = (card: CardModel, index: number) =>
    card.parentId === id && (
      <Card key={card.id} colIndex={colIndex} cardIndex={index} {...card} />
    )

  return (
    <DroppableColumnWrapper colIndex={colIndex}>
      <AddButton
        customClass={stl.addCardButton}
        onButtonClick={addCard}
        name="add-card"
        size={25}
      />
      <div className={stl.title}>{description}</div>
      {cards.map(renderCards)}
    </DroppableColumnWrapper>
  )
}

export default Column

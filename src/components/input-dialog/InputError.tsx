import clsx from 'clsx'

import stl from './InputDialog.module.scss'

interface ErrorProps {
  errors: { [key: string]: string }
  touched: { [key: string]: boolean }
  fieldId: string
  customClass?: string
}

const InputError = ({ errors, touched, fieldId, customClass }: ErrorProps) => (
  <div className={clsx(stl.inputError, customClass)}>
    {errors[fieldId] && touched[fieldId] && errors[fieldId]}
  </div>
)

export default InputError

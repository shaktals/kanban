import { useFormik, FormikErrors } from 'formik'
import { useDispatch } from 'react-redux'
import clsx from 'clsx'

import LoadingIndicator from 'components/loading'
import InputError from './InputError'
import CloseIcon from 'assets/close.svg'

import { ActionCreator, Action, ModalType, TruthyString } from 'lib/models'
import { entityTypes } from 'lib/constants'
import { useDisplayState } from 'lib/hooks'
import { cardTypes } from 'store/card'
import { columnTypes } from 'store/column'

import stl from './InputDialog.module.scss'
import { displayTypes } from 'store/display'

interface FormValues {
  description?: string
  submitButton?: string
}

const validateForm = ({ description }: FormValues) => {
  const errors: FormikErrors<FormValues> = {}

  // On a prod setup, more specific validation could be added here
  // We could check for str length, already existing titles, etc.
  if (!description) errors.description = 'Description required.'

  return errors
}

interface InputProps {
  inputType: ModalType
  parentId?: TruthyString
  parentDescription?: TruthyString
  action: ActionCreator | ((arg: FormValues) => Action)
}

const InputDialog = ({
  inputType,
  parentId,
  parentDescription,
  action,
}: InputProps) => {
  const dispatch = useDispatch()
  const isCard = inputType === entityTypes.CARD

  const onCloseClick = () =>
    dispatch({ type: displayTypes.SHOW_MODAL, payload: false })

  const {
    values,
    handleSubmit,
    handleChange,
    handleBlur,
    isValid,
    errors,
    touched,
  } = useFormik({
    initialValues: {
      description: '',
      // Trick formik 'touched' to disable create button only when button has been touched already
      // This is a personal UX
      submitButton: '',
    },
    validate: validateForm,
    onSubmit: ({ description }) => {
      dispatch(action({ description, parentId }) as Action)
    },
  })

  const actionType = isCard ? cardTypes.CREATE_CARD : columnTypes.CREATE_COLUMN
  const { error, isLoading, message } = useDisplayState({ actionType })

  const showDisabledState = touched.submitButton || touched.description
  const disableButton = showDisabledState && (!isValid || isLoading)

  return (
    <form className={stl.container} onSubmit={handleSubmit}>
      <button type="button" className={stl.closeButton} onClick={onCloseClick}>
        <CloseIcon className={stl.closeIcon} />
      </button>
      <div className={stl.titleContainer}>
        <div className={stl.formTitle}>Create {inputType}</div>
        {isCard && (
          <div className={stl.columnTitle}>Column: {parentDescription}</div>
        )}
      </div>

      <div className={stl.formGroup}>
        <label className={stl.label} htmlFor="description">
          Description
        </label>
        <input
          id="description"
          name="description"
          type="text"
          className={stl.textInput}
          onChange={handleChange}
          onBlur={handleBlur}
          value={values.description}
        />
        <InputError errors={errors} touched={touched} fieldId="description" />
      </div>

      <div className={stl.buttonContainer}>
        <button
          type="submit"
          className={clsx(stl.submitButton, disableButton && stl.disabled)}
          disabled={disableButton}
        >
          Create
        </button>
        {isLoading && <LoadingIndicator show={true} />}
        {error && <div className={stl.apiError}>Djou {error}</div>}
        {message && <div className={stl.apiSuccess}>{message}</div>}
      </div>
    </form>
  )
}

export default InputDialog

import {
  MouseEventHandler,
  useState,
  useRef,
  ChangeEventHandler,
  FocusEventHandler,
} from 'react'
import { useDispatch } from 'react-redux'

import EditIcon from 'assets/edit.svg'
import SaveIcon from 'assets/save.svg'

import { updateCard } from 'store/card'
import { cardTypes } from 'store/card'
import { Action } from 'lib/models'

import stl from './Card.module.scss'

interface CardContentProps {
  id: string
  description: string
  cardIndex: number
}

const CardContent = ({ id, description, cardIndex }: CardContentProps) => {
  const dispatch = useDispatch()
  const contentRef = useRef<HTMLInputElement>(null)
  const [editMode, setEditMode] = useState(false)

  const dispatchUpdateCard = () => {
    const payload = { description, cardIndex }
    dispatch(updateCard({ description, id }) as Action)
  }

  const onEditClick: MouseEventHandler = e => {
    e.preventDefault()
    e.stopPropagation()
    setEditMode(state => {
      if (!state && contentRef.current) contentRef.current.focus()
      else dispatchUpdateCard()

      return !state
    })
  }

  const onInputBlur: FocusEventHandler = () => {
    if (editMode) dispatchUpdateCard()
    setEditMode(false)
  }

  const onCardChange: ChangeEventHandler = e => {
    e.preventDefault()
    e.stopPropagation()
    const { value } = e.target as HTMLInputElement
    const payload = { value, cardIndex }

    dispatch({ type: cardTypes.EDIT_CARD, payload })
  }

  return (
    <div className={stl.card}>
      <input
        type="text"
        ref={contentRef}
        className={stl.description}
        value={description}
        onChange={onCardChange}
        readOnly={!editMode}
        onBlur={onInputBlur}
      />
      <button className={stl.editButton} onClick={onEditClick}>
        {!editMode && <EditIcon className={stl.editIcon} />}
        {editMode && <SaveIcon className={stl.editIcon} />}
      </button>
    </div>
  )
}

export default CardContent

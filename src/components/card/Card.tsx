import type { Identifier, XYCoord } from 'dnd-core'
import { useRef } from 'react'
import { useDispatch } from 'react-redux'
import { useDrag, useDrop, DragSourceMonitor } from 'react-dnd'
import clsx from 'clsx'

import CardContent from './CardContent'

import { updateCard, reorderCardDisplay } from 'store/card'

import { cardTypes } from 'lib/constants'
import { CardModel, Action, DragItem, GenericEntity } from 'lib/models'

import stl from './Card.module.scss'

interface CardProps extends CardModel {
  colIndex: number
  cardIndex: number
}

const Card = ({ id, description, colIndex, cardIndex }: CardProps) => {
  const ref = useRef<HTMLDivElement>(null)
  const dispatch = useDispatch()

  const [{ isDragging }, drag] = useDrag<
    DragItem,
    void,
    { isDragging: boolean }
  >(
    () => ({
      type: cardTypes.TASK,
      item: { cardId: id, index: cardIndex, colIndex },
      collect: (monitor: DragSourceMonitor) => ({
        isDragging: !!monitor.isDragging(),
      }),
      end: (item, monitor) => {
        if (!monitor.didDrop()) return

        const { colIndex: newIndex, newCardIndex } =
          monitor.getDropResult() as GenericEntity

        const payload: GenericEntity = { id, newCardIndex }

        // Need to check if the card was moved to a different column
        // and if 'REORDER_CARD' hasn't taken care of it already
        if (newIndex !== colIndex && newIndex !== item.colIndex)
          payload.newColIndex = newIndex

        dispatch(updateCard(payload) as Action)
      },
    }),
    [cardIndex, colIndex]
  )

  const [{ handlerId, draggedItem }, drop] = useDrop<
    DragItem,
    void,
    { handlerId: Identifier | null; draggedItem: DragItem | null }
  >(
    () => ({
      accept: cardTypes.TASK,
      collect: monitor => ({
        handlerId: monitor.getHandlerId(),
        draggedItem: monitor.getItem(),
      }),
      hover: (item: DragItem, monitor) => {
        if (!ref.current) return

        const { index: dragIndex, colIndex: dragColIndex } = item
        const hoverIndex = cardIndex

        // Don't replace items with themselves
        if (dragIndex === hoverIndex && dragColIndex === colIndex) return

        // Determine rectangle on screen
        const hoverBoundingRect = ref.current?.getBoundingClientRect()

        // Get vertical middle
        const hoverMiddleY =
          (hoverBoundingRect.bottom - hoverBoundingRect.top) / 2

        // Determine mouse position
        const clientOffset = monitor.getClientOffset()

        // Get pixels to the top
        const hoverClientY = (clientOffset as XYCoord).y - hoverBoundingRect.top

        // Only perform the move when the mouse has crossed half of the items height
        // When dragging downwards, only move when the cursor is below 50%
        // When dragging upwards, only move when the cursor is above 50%

        // Dragging downwards
        if (dragIndex < hoverIndex && hoverClientY < hoverMiddleY) return

        // Dragging upwards
        if (dragIndex > hoverIndex && hoverClientY > hoverMiddleY) return

        // Time to actually perform the action
        const payload = { dragIndex, hoverIndex, dragColIndex, colIndex }
        dispatch(reorderCardDisplay(payload) as Action)

        // These are a lib author's note:
        // "Note: we're mutating the monitor item here!
        // Generally it's better to avoid mutations,
        // but it's good here for the sake of performance
        // to avoid expensive index searches."
        // My comment:
        // This seems to be a lib pitfall,
        // but I haven't seen any issues arise from it
        item.index = hoverIndex
        item.colIndex = colIndex
      },
    }),
    [colIndex, cardIndex]
  )

  drag(drop(ref))

  const hide = draggedItem?.cardId === id || isDragging

  return (
    <div
      className={clsx(hide && stl.dragging)}
      data-handler-id={handlerId}
      ref={ref}
    >
      <CardContent id={id} description={description} cardIndex={cardIndex} />
    </div>
  )
}

export default Card

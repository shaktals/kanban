import type { NextPage } from 'next'
import { useSelector } from 'react-redux'

import DndWrapper from 'components/dnd-wrapper'
import Column from 'components/column'
import Footer from 'components/footer'
import KanbanHead from './KanbanHead'

import { selectColumns } from 'store/column'

import stl from './Kanban.module.scss'

const Kanban: NextPage = () => {
  const columns = useSelector(selectColumns)

  return (
    <DndWrapper>
      <div className={stl.container}>
        <KanbanHead />
        <div className={stl.columns}>
          {columns.map(({ id, description }, index) => (
            <Column
              key={id}
              id={id}
              description={description}
              colIndex={index}
            />
          ))}
        </div>
        <Footer />
      </div>
    </DndWrapper>
  )
}

export default Kanban

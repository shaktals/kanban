import Head from 'next/head'
import Header from 'components/header'

const KanbanHead = () => {
  return (
    <>
      <Head>
        <title>Kanban Manager</title>
        <meta
          name="description"
          content="Kanban style Drag'n'drop Project Management"
        />
        <link rel="icon" href="/hash.ico" />
      </Head>
      <Header />
    </>
  )
}

export default KanbanHead
